x = {
    'RB 16': {
         'lengths': [12000, 6500],
         'items': [
             {
                 'length': 1500.0,
                 'qty': 16,
                 'item_no': '1.1, 1.2, 2.1, 2.2'
             }, 
             {
                 'length': 7000.0,
                 'qty': 4,
                 'item_no': '1.3'
             },
             {
                 'length': 14000.0,
                 'qty': 4,
                 'item_no': '2.3'
             }
         ]
    }
}