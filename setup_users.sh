docker exec -it <CONTAINER> runuser -l postgres -c 'createuser -d -p api'
docker exec -it <CONTAINER> runuser -l postgres -c 'createdb -O api api'

docker exec -it <CONTAINER> runuser -l postgres -c 'createuser -d -p keycloak'
docker exec -it <CONTAINER> runuser -l postgres -c 'createdb -O keycloak keycloak'
